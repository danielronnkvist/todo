import "./Todos.css";
import { useQuery, gql } from "@apollo/client";

const GET_TODOS = gql`
{
    Todos {
        title,
        id
    }
}
`;

function Todos() {
  const { loading, error, data } = useQuery(GET_TODOS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :( <span>{error.message}</span></p>;
  const todos = data.Todos.map((todo: any) => {
    return <li key={todo.id}>{todo.title}</li>;
  });
  return (
    <div>
      <ul>{todos}</ul>
    </div>
  );
}

export default Todos;
