import "./App.css";
import Todos from "./Todos/Todos";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Todos</h1>
      </header>
      <Todos />
    </div>
  );
}

export default App;
