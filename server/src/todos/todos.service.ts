import { Injectable } from '@nestjs/common';
import { NewTodoInput } from './dto/new-todo-input';
import { Todo } from './todo.model';

// TODO: Replace with a real database
let todos: Todo[] = [
  {
    title: 'Todo 1',
    createdBy: 'user1',
    id: '1',
    creationDate: new Date(),
  },
];

@Injectable()
export class TodosService {
  async create(data: NewTodoInput): Promise<Todo> {
    const todo = {
      ...data,
      id: `${todos.length + 1}`,
      creationDate: new Date(),
    };
    todos.push(todo);
    return todo;
  }

  async findOneById(id: string): Promise<Todo | undefined> {
    return todos.find((todo) => todo.id === id);
  }

  async findAll(): Promise<Todo[]> {
    return todos as Todo[];
  }

  async remove(id: string): Promise<boolean> {
    todos = todos.filter((todo) => todo.id !== id);
    return true;
  }
}
