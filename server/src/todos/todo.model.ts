import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'todo ' })
export class Todo {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Field((type) => ID)
  id: string;

  @Field()
  title: string;

  // An improved version would reference a User id
  @Field()
  createdBy: string;

  @Field()
  creationDate: Date;
}
