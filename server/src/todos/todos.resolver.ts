import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { NewTodoInput } from './dto/new-todo-input';
import { Todo } from './todo.model';
import { TodosService } from './todos.service';

@Resolver(() => Todo)
export class TodosResolver {
  constructor(private readonly todosService: TodosService) {}

  @Query(() => Todo)
  async Todo(@Args('id') id: string): Promise<Todo> {
    const Todo = await this.todosService.findOneById(id);
    if (!Todo) {
      throw new NotFoundException(id);
    }
    return Todo;
  }

  @Query(() => [Todo])
  Todos(): Promise<Todo[]> {
    return this.todosService.findAll();
  }

  @Mutation(() => Todo)
  async addTodo(@Args('newTodoData') newTodoData: NewTodoInput): Promise<Todo> {
    const Todo = await this.todosService.create(newTodoData);
    return Todo;
  }

  @Mutation(() => Boolean)
  async removeTodo(@Args('id') id: string) {
    return this.todosService.remove(id);
  }
}
