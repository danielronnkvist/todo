import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class NewTodoInput {
  @Field()
  title: string;

  @Field()
  createdBy: string;
}
